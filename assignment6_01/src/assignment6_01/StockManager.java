package assignment6_01;

//import assignment6_01.ElectronicsStock;
public interface StockManager {

	void addStock();
	void purchaseProduct();
	void displayStock();
}
