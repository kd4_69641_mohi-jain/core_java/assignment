package assignment6_01;

import java.util.Scanner;
import assignment6_01.Electronics.*;
import assignment6_01.ElectronicsException;
public class Tv extends Electronics{
	
	private int screen_inches;
	private int pixel_density;
	

	@Override
	public void acceptData() {
		System.out.println("Enter Tv Data = ");
		accept();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the screen size");
		 this.screen_inches = sc.nextInt();
		 System.out.println("Enter the pixel density");
		 this.pixel_density = sc.nextInt();
		 if(this.screen_inches <= 0 || this.pixel_density <= 0)
				throw new ElectronicsException();
	}
	@Override
	public void printData() {
		print();
		System.out.println("Details for Tv :");
		System.out.println("Screen size is:"+this.screen_inches);
		System.out.println("pixel density is:"+this.pixel_density);
	}
	@Override
	public void correctData() {
		Scanner sc = new Scanner(System.in);
		while (this.screen_inches <= 0) {
			System.out.println("Please enter correct Screen size = ");
			this.screen_inches = sc.nextInt();
		}
		while (this.pixel_density <= 0) {
			System.out.println("Please enter correct Pixel = ");
			this.pixel_density = sc.nextInt();
		}
	}

	
	

}
