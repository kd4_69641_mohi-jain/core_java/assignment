package assignment6_01;

public class ElectronicsException extends RuntimeException{
private String message;
	
	public ElectronicsException() {
		this.message = "screen inches,pixel density and capacity cannot be 0 or less than 0";
	}
	
	public ElectronicsException(String message) {
		super(message);
		this.message = message;
	}

	public void printTrace() {
		System.out.println("ElectronicsException :- ");
		System.out.println("Error - " + this.message);
	}
	
}
