package assignment6_01;

import java.util.Scanner;
import assignment6_01.Electronics.*;
import assignment6_01.MobileException;

public class Mobile extends Electronics {

	private int ram;
	private int storage;
	@Override
	public void acceptData() throws MobileException {
		 System.out.println("Enter Mobile Data = ");
		 accept();
		 Scanner sc = new Scanner(System.in);
		 System.out.println("Enter the ram");
		 this.ram = sc.nextInt();
		 System.out.println("Enter the storage");
		 this.storage = sc.nextInt();
		 if(this.ram <= 0 && storage>0 )
			 throw new MobileException("ram cannot be 0");
		 if(this.storage <= 0 && ram>0)
			 throw new MobileException("storage cannot be 0");
		 if(this.storage <= 0 && ram<=0)
			 throw new MobileException("ram and storage cannot be 0");
		 
	}
	@Override
	public void printData(){
		print();
		System.out.println("Details for Mobile phone :");
		System.out.println("Ram is:"+this.ram);
		System.out.println("storage is:"+this.storage);
	}
	
	@Override
	public void correctData() {
		Scanner sc = new Scanner(System.in);
		while (this.ram <= 0) {
			System.out.println("Please enter correct Ram size = ");
			this.ram = sc.nextInt();
		}
		while (this.storage <= 0) {
			System.out.println("Please enter correct Storage = ");
			this.storage = sc.nextInt();
		}
	}

	
}
