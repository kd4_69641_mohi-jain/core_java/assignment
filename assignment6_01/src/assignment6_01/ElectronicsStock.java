package assignment6_01;

import assignment6_01.ElectronicsException;
import assignment6_01.MobileException;
import java.util.Scanner;

public class ElectronicsStock implements StockManager {

	Electronics item;
    int quantity;
	
	

	@Override
	public void addStock() {
		try
		{
			this.item.acceptData();
		}
		catch(MobileException e)
		{
			e.printTrace();
			this.item.correctData();
		}
		System.out.println("Enter quantity :");
		this.quantity=new Scanner (System.in).nextInt();
	}
	@Override
	public void purchaseProduct() throws ElectronicsException 
	{
		if(this.quantity==0)
		      throw new ElectronicsException ("out of stock");
		else
			this.quantity--;
	}
	@Override
	public void displayStock() {
		System.out.println("*************************");
		this.item.printData();
		System.out.println("Quantity = " + this.quantity);
		System.out.println("*************************");
	}
	
}