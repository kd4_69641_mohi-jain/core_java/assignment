package assignment6_01;

//import java.util.Arrays;

public class MobileException extends Exception{

	private String message;
	public MobileException() {
		this.message = "Ram or storage for mobile cannot be zero or less than zero";
	}
	public MobileException(String message) {
		this.message = message;
		System.out.println(message);
	}

	public void printTrace() {
		System.out.println("MobileException :- ");
		System.out.println("Error - " + this.message);
	}

		
}
	

