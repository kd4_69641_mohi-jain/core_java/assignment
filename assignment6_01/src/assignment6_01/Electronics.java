package assignment6_01;

import java.util.Scanner;
import assignment6_01.MobileException;
public abstract class Electronics {

	private String model;
	private String description;
	private double price;
        
	public void accept()
	{
	     Scanner sc = new Scanner(System.in);
	     System.out.println("Enter the model name");
	     this.model= sc.next();
	     System.out.println("Enter the description");
	     this.model= sc.next();
	     System.out.println("Enter the price");
	     this.price=sc.nextDouble();
	}
	public abstract void acceptData() throws MobileException ;
	public void print()
	{
		System.out.println("Model is :"+this.model);
		System.out.println("Description is: "+this.description);
		System.out.println("Price is:"+this.price);
	}
	public abstract void printData();
	public abstract void correctData();

}
