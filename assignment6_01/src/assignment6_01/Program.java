package assignment6_01;
import java.util.Scanner;
import assignment6_01.Mobile;
import assignment6_01.Tv;
import assignment6_01.WashingMachine;
import assignment6_01.Electronics.*;

	public class Program {
		private static ElectronicsStock stockarr[];

		private static int menu() {
			System.out.println("0.Exit");
			System.out.println("1.Enter the products");
			System.out.println("2.Display the products");
			System.out.println("3.Purchase the product");
			System.out.println("Enter Your Choice");
			return new Scanner(System.in).nextInt();
		}

		private static void initArray() {
			// Declaring array of Stock
			stockarr = new ElectronicsStock[3];

			// Creating objects of stock to keep Products and quantity
			for (int i = 0; i < stockarr.length; i++) {
				stockarr[i] = new ElectronicsStock();

				// adding all products object inside stock object
				if (i == 0)
					stockarr[i].item = new Mobile();
				if (i == 1)
					stockarr[i].item = new Tv();
				if (i == 2)
					stockarr[i].item = new WashingMachine();
			}
		}

		public static void main(String[] args) {
			int choice;
			while ((choice = menu()) != 0) {
				switch (choice) {
				case 1:
					initArray();
					for (ElectronicsStock stock : stockarr) {
						stock.addStock();
					}
					break;
				case 2:
					for (ElectronicsStock stock : stockarr) {
						stock.displayStock();
					}
					break;
				case 3:
					System.out.println("What do you wish to Purchase ? ");
					System.out.println("1. Mobile, 2.TV, 3.WashingMachine");
					int choice2 = new Scanner(System.in).nextInt();
					if (choice2 == 1 || choice2 == 2 || choice2 == 3)
						stockarr[choice2 - 1].purchaseProduct();
					else
						System.out.println("Wrong Choice ... :(");
					break;
				default:
					System.out.println("Wrong Choice... :(");
					break;
				}
			}

		}

	}

