package assignment6_01;

import java.util.Scanner;
import assignment6_01.Electronics.*;
import assignment6_01.ElectronicsException;


public class WashingMachine extends Electronics {
	private int capacity;
	private String type;
	
//	public int getCapacity() {
//		return capacity;
//	}
//	public void setCapacity(int capacity) {
//		this.capacity = capacity;
//	}
//	public String getType() {
//		return type;
//	}
//	public void setType(String type) {
//		this.type = type;
//	}
	@Override
	public void acceptData() {
		System.out.println("Enter Washing Machine Data = ");
	        accept();
		    Scanner sc = new Scanner(System.in);
			System.out.println("Enter capacity = ");
			this.capacity = sc.nextInt();
			System.out.println("Enter type semi/auto = ");
			this.type = sc.next();
		 if(this.capacity <= 0)
				throw new ElectronicsException();
	}
	@Override
	public void printData(){
		print();
		System.out.println("Details for Washing Machine :");
		System.out.println("Capacity is:"+this.capacity);
		System.out.println("Type is:"+this.type);
		}
	@Override
	public void correctData() {
		Scanner sc = new Scanner(System.in);
		while (this.capacity <= 0) {
			System.out.println("Please enter correct capacity = ");
			this.capacity = sc.nextInt();
		}
	}

}
