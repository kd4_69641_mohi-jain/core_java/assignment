package student;
import student.StudentSort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class StudentComparator implements Comparator<StudentSort> {

	@Override
	public int compare(StudentSort s1, StudentSort s2) {
		int diff = s1.getCity().compareTo(s2.getCity());
		if(diff==0)
		{
			if(s1.getMarks()>s1.getMarks())
			{
				return -1;
			}
			if(s1.getMarks()<s1.getMarks())
			{
				return +1;
			}
			else
			{
				diff =  s1.getName().compareTo(s2.getName());
				return diff;
				
			}
		}
		
		
		return -diff;
	}
}

public class Program {

	public static void main(String[] args) {
		Scanner name = new Scanner(System.in);

		StudentSort[] stu = new StudentSort[] { 
		new StudentSort(1, "Bharatpur", "Mohi", 600), 
		new StudentSort(2, "Agra", "Mona", 400),
		new StudentSort(3, "Jaipur", "payal", 660),
		new StudentSort(4, "Bhopal", "syalee", 600), 
		new StudentSort(5, "Delhi", "monisha", 600) };

		for (StudentSort student : stu) {
			System.out.println(student);
		}
		System.out.println("****************8");
		Arrays.sort(stu, new StudentComparator());
		for (StudentSort e : stu)
			System.out.println(e);
		System.out.println();
	}

}