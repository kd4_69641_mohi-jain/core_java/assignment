package student;

public class StudentSort {
	private int roll;
	 private String name;
	 private String city;
	 private double marks;
	//Constructor
	 public StudentSort(int roll, String city, String name, double marks) {
			super();
			this.roll = roll;
			this.name = name;
			this.city = city;
			this.marks = marks;
		}
	 //'''' get set methods
	public int getRoll() {
		return roll;
	}
	public void setRoll(int roll) {
		this.roll = roll;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public double getMarks() {
		return marks;
	}
	public void setMarks(double marks) {
		this.marks = marks;
	}
	
	
	@Override
	public String toString() {
		return "Student [roll=" + roll +", city=" + city+ ", name=" + name +  ", marks=" + marks + "]";
	}
	
	
	
	
}