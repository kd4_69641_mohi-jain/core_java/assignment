package assignment7_01;

public class Maximum {
    public static <T> void printdata(T[] arr)
    {
    	for(T a : arr)
    		System.out.println(a);
    	System.out.println("Number of elements entered :"+arr.length);
    	
    }
	public static <T extends Number> T getMax(T[] arr)
	{
		T max=arr[0];
		for(T num: arr) {
			if(num.doubleValue()>max.doubleValue())
				max=num;
		}
		return max;
	}
	
	public static void main(String[] args) {
	Integer[] arr2 = {10,20,30,40};
	printdata(arr2);
    int maxInt = getMax(arr2);
    System.out.println("Min :" + maxInt);
	}

}
