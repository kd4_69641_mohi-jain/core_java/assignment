package assignment7_01;

public class Minimum {
	public static <T> void printArray(T[] arr) {
        for(T ele : arr)
            System.out.println(ele);
        System.out.println("Number of elements printed: " + arr.length);
    }
	 public static <T extends Number> T getMin(T[] arr) {
	    	T min = arr[0];
	    	for(T num:arr) {
	    		if(num.doubleValue() < min.doubleValue())
	    			min = num;
	    	}
	    	return min;
	    }
	public static void main(String[] args) {
		Integer[] arr2 = { 1,2,3,4,5,6,7,8 };
	    printArray(arr2); 	
	    int minInt = getMin(arr2);
	    System.out.println("Min : " + minInt);
	}

}
