package assignment7_01;

import java.util.Scanner;

class Square extends Shape {
	private int side;

	public Square() {
		this.side = 0;
	}
	public double calarea() {
		this.Area= (this.side * this.side);
        return Area;
	}
	public void acceptval(){
		System.out.println("Enter side = ");
		Scanner c = new Scanner(System.in);
		this.side = c.nextInt();

	}

}
