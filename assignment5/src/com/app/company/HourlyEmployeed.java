package com.app.company;

public class HourlyEmployeed extends Employee {
	private double wage;
	private double hours;
	public HourlyEmployeed(String firstname,String lastname,String ssn,double wage,double hours)
	{
		super(firstname,lastname,ssn);
		this.setHours(hours);
		this.setWage(wage);
	}
	public double getWage() {
		return wage;
	}
	public void setWage(double hourlywage) {
		if(hourlywage>=0.0)
		{
		this.wage = wage;
		}
		else
			throw new IllegalArgumentException("hourly wage must be>=0.0");
	}
	public double getHours() {
		return hours;
	}
	public void setHours(double hourworked) {
		if(hourworked>=0.0)
		{
		this.hours = hours;
		}
		else
			throw new IllegalArgumentException("hour must be>=0.0 and <=168");
		//work hour per week 7*24
	}
	@Override
	public double earnings() {
		if(getHours()<=40)
		{
		return getWage()*getHours();
		}
		else
			return getWage()*getHours()+(getHours()-40)*getWage()*1.5;
	}
    @Override
	public String toString() {
		return String.format("hourly employee : %s\n%s:$%,.2f; %s: %, .2f", super.toString(),"hourly wage",getWage(),"hours worked",getHours());
	}
	

}
