package com.app.company;

public class Main {
    public static SalariedEmployee salariedemployee;
    public static HourlyEmployeed hourlyemployeed;
    public static CommissionEmployee commissionemployee ;
    public static BasePlusCommissionEmployee basepluscommissionemployee;
	public static void main(String[] args) {
		Employee[] employees= new Employee[4];
		employees[0]= new SalariedEmployee("mohi","jain","A001",80000.00);
		employees[1]= new SalariedEmployee("mona","sharma","A002",60000.00);
		employees[2]= new SalariedEmployee("saylee","pawar","A003",45000.00);
		employees[3]= new SalariedEmployee("payal","dhanayat","A004",50000.00);
		
		System.out.println("employees processed polymorphically:\n");
		
		for(Employee currentemployee : employees)
		{
			System.out.println(currentemployee);
			if(currentemployee instanceof BasePlusCommissionEmployee)
			{
				BasePlusCommissionEmployee employee = (BasePlusCommissionEmployee) currentemployee;
				
				employee.setBasesalary(1.10*employee.getBasesalary());
			}
			System.out.printf("earned $%,.2f\n\n",currentemployee.earnings());
		}
		}
}
	


