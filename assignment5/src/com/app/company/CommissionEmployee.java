package com.app.company;

public class CommissionEmployee extends Employee {
     private double grossSales;
     private double commissionRate;
     public CommissionEmployee(String firstname,String lastname,String ssn,double sales,double rate)
     {
    	 super(firstname,lastname,ssn);
    	 this.setCommissionrate(rate);
    	 this.setGrosssales(sales);
     }
	public double getGrosssales() {
		return grossSales;
	}
	public void setGrosssales(double sales) {
		if(sales>=0.0)
		{
		this.grossSales = grossSales;
		}
		else
			throw new IllegalArgumentException("gross sales must be>0.0");
	}
	public double getCommissionrate() {
		return commissionRate;
	}
	public void setCommissionrate(double rate) {
		if(rate>0.0 && rate<1.0)
		{
		this.commissionRate = commissionRate;
		}
		else
			throw new IllegalArgumentException("comission rate must be >0 and < 1.0");
	}
	@Override
	public double earnings() {
		return getGrosssales()*getCommissionrate();
	}
	@Override
	public String toString() {
        return String.format("%s: %s\n%s: $%,.2f; %s: %.2f","commission employee ",super.toString(),"gross sales",getGrosssales(),"commission rate",getCommissionrate());
	}
	


}

