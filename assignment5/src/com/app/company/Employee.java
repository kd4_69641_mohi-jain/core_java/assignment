package com.app.company;

abstract class Employee
{
	private String firstname;
	private String lastname;
	private String socialsecuritynumber;
	
	public Employee(String firstname,String lastname,String ssn)
	{
		this.firstname=firstname;
		this.lastname=lastname;
		this.socialsecuritynumber= ssn;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSocialsecuritynumber() {
		return socialsecuritynumber;
	}

	public void setSocialsecuritynumber(String ssn) {
		this.socialsecuritynumber = ssn;
	}

	public abstract double earnings();
	@Override
	public String toString() {
        return String.format("%s %s ssn: %s",getFirstname(),getLastname(),getSocialsecuritynumber()); 
	}
	
}



