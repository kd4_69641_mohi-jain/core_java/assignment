package com.app.company;

public class BasePlusCommissionEmployee extends CommissionEmployee {

	private double basesalary;
	public BasePlusCommissionEmployee(String firstname,String lastname,String ssn,double sales,double rate,double salary)
	{
		super(firstname,lastname,ssn,sales,rate);
		this.setBasesalary(salary);
	}
	public double getBasesalary() {
		return basesalary;
	}
	public void setBasesalary(double salary) {
		if(salary>=0.0)
		{
		this.basesalary = salary;
		}
		else
			throw new IllegalArgumentException("Base salary must be>0.0");
	}
	
	@Override
	public double earnings() {
		return getBasesalary()+super.earnings();
	}
	@Override
	public String toString() {
		return String.format("%s %s; %s: $%,.2f","base-salaried",super.toString(),"base salary",getBasesalary());
	}
}
